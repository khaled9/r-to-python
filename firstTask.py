# Loading the required libraries.
import pandas as pd
from pyhive import hive
import datetime
import time
#connect with Hive

conn = hive.Connection(host="YOUR_HIVE_HOST", port=PORT, username="YOU")
# Load customer data from HIVE data mart.
customer_data_mart_df = pd.read_sql("Select * from insights_data.customer_data_mart", conn)
#filter data
activeCustomer = customer_data_mart_df[(customer_data_mart_df["is_retail"]=="Y") & (customer_data_mart_df["31mar_flag"]=="N")]
#Type conversion of Customer_Start_Date to date type.
activeCustomer['customer_start_date'] = pd.to_datetime(activeCustomer['customer_start_date'])
# Identifying number of active months for a customer. (since latest date considered for 18 months period.)
numOfAciveMonth = len(activeCustomer[(customer_start_date > "2018-07-01")])
# Filtering only the required customer demographics to be used by model. 
customerDemographic = activeCustomer[["dim_customer_id","age","gender","education_level_desc","customer_program_d",
                               "years_with_bank","has_internet_banking","has_mobile_banking","has_phone_bank",
                               "has_visa_electron","is_inactive"]]

# Imputing the missing ages customer program wise.
MASS = customerDemographic['age'][(customerDemographic.isnull()) & (customerDemographic['customer_program_d'] == 'MASS')]
massMean = MASS.mean()
customerDemographic['age'][customerDemographic['customer_program_d']=='MASS'].fillna(massMean, inplace=True)


ELITE = customerDemographic['age'][customerDemographic.isnull() & customerDemographic['customer_program_d'] == 'ELITE']
eliteMean = ELITE.mean()
customerDemographic['age'][customerDemographic['customer_program_d']=='ELITE'].fillna(eliteMean, inplace=True)

PREMIUM = customerDemographic['age'][customerDemographic.isnull() & customerDemographic['customer_program_d'] == 'PREMIUM']
premiumMean = PREMIUM.mean()
customerDemographic['age'][customerDemographic['customer_program_d']=='PREMIUM'].fillna(premiumMean, inplace=True)

SHABAB = customerDemographic['age'][customerDemographic.isnull() & customerDemographic['customer_program_d'] == 'SHABAB']
shababMean = SHABAB.mean()
customerDemographic['age'][customerDemographic['customer_program_d']=='SHABAB'].fillna(shababMean, inplace=True)

ARABIEXTRA = customerDemographic['age'][customerDemographic.isnull() & customerDemographic['customer_program_d'] == 'ARABI EXTRA']
arabExtraMean = ARABIEXTRA.mean()
customerDemographic['age'][customerDemographic['customer_program_d']=='ARABI EXTRA'].fillna(arabExtraMean, inplace=True)


# Imputing the missing education levels as "OTHERS".

customerDemographic['education_level_desc'][(customerDemographic['customer_program_d'].isnull())].fillna("OTHER", inplace=True)




