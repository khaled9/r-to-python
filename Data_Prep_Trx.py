# Loading the required libraries.
import pandas as pd
from firstTask import customerDemographic
from pyhive import hive

#connect with Hive
conn = hive.Connection(host="YOUR_HIVE_HOST", port=PORT, username="YOU")
# Load customer data from HIVE data mart.
customer_data_mart_df = pd.read_sql("Select * from insights_data.transaction_data_mart", conn)
# Type conversion of DIM_DATE_ID to date type.
customer_data_mart_df['dim_date_id'] = pd.to_datetime(customer_data_mart_df['dim_date_id'])
# Filtering transactions for last 18 months.
customerTransactions = customer_data_mart_df[(customer_data_mart_df['dim_date_id'] > "2017-01-01")]
# Filtering customer initiated transactions for Channels - ATM, ITM, BRANCH, INTERNET, MOBILE, PHONE BANKING & POS (CASH ADVANCE only) 
customerTransactions = customerTransactions[(customerTransactions['channel']=="ATM") | (customerTransactions['channel']=="INTERNET BANKING") |
                                            (customerTransactions['channel']=="ITM") | (customerTransactions['channel']=="MOBILE BANKING") |
                                            (customerTransactions['channel']=="BRANCH") | (customerTransactions['channel']=="PHONE BANKING") |(customerTransactions['channel']=="POS"& customerTransactions["activity_type"]=="CASH ADVANCE") 
]
customerTransactions[customerTransactions["channel"]=="POS"] = "ATM"
customerTransactions = customerTransactions[customerTransactions['customer_initiated_indicator']=="Y"]
# Categorizing the activity type irrespective of channel.
customerTransactions['activity_type'] = customerTransactions.replace("ATM",customerTransactions["activity_type"])
customerTransactions['activity_type'] = customerTransactions.replace("IB",customerTransactions["activity_type"])
customerTransactions['activity_type'] = customerTransactions.replace("BRANCH",customerTransactions["activity_type"])
customerTransactions['activity_type'] = customerTransactions.replace("MB",customerTransactions["activity_type"])
customerTransactions['activity_type'] = customerTransactions.replace("ITM",customerTransactions["activity_type"])
customerTransactions['activity_type'] = customerTransactions.replace("PB",customerTransactions["activity_type"])

customerTransactions[customerTransactions["channel"]=="DEPOSIT"] = "CASH DEPOSITM"
customerTransactions[customerTransactions["channel"]=="WITHDRAWAL"] = "CASH WITHDRAWAL"
customerTransactions[(customerTransactions["channel"]=="ECC CHEQUES") | (customerTransactions["channel"]=="CHQ DEPOSIT") ] = "CHEQUE DEPOSIT"
customerTransactions[customerTransactions["channel"]=="CHQ WITHDRAWAL"] = "CHEQUE WITHDRAWAL"
customerTransactions[customerTransactions["channel"]=="EFAWATEERCOM"] = "E-FAWATEERCOM"
customerTransactions[customerTransactions["channel"]=="TRANSFER"] = "TRANSFERS"
customerTransactions[customerTransactions["channel"]=="CREDIT PAYMENT"] = "CREDIT CARD PAYMENT"
customerTransactions[customerTransactions["channel"]=="E-FAWATEERCOM"] = "BILL PAYMENT"

# Deriving additional features on Transactions data, to be used for modelling.
customerDreiveAdditional = customerTransactions.groupby("dim_customer_id")
n_distinct = customerDreiveAdditional.dim_customer_id
counts = customerDreiveAdditional["dim_customer_id"].count()
last_date_df = pd.DataFrame()
for id, group in customerDreiveAdditional:
    df_dict = {}
    df_dict['Total_Transactions'] = counts[id]
    df_dict['Total_Branch_Transactions'] = len(group[(group['channel']=="ATM")])
    df_dict['Total_SS_Transactions'] = len(group[(group['channel']=="ATM") | (group['channel']=="ITM") |
                                        (group['channel']=="INTERNET BANKING") | (group['channel']=="MOBILE BANKING") | (group['channel']=="PHONE BANKING") ])

    df_dict['Branch_Visit_Freq_Monthly'] = group['dim_date_id'][(group['channel']=='BRANCH')].nunique()
    df_dict['Unique_Branches_Visited'] = group['composite_branch_atm_id'][(group['channel']=='BRANCH')].nunique()
    if df_dict['Total_Branch_Transactions'] == 0:
        df_dict['Channel_Usage'] = "Self Service Only"
    else:
        if df_dict['Total_SS_Transactions'] == 0:
            df_dict['Channel_Usage'] = 'Branch Only'
        else:
            df_dict['Channel_Usage'] = 'Both'





    df_dict['Branch_Usage_ratio'] = df_dict['Total_Branch_Transactions']/df_dict['Total_Transactions']

    df_dict['Total_Online_Activities'] = len(group[(group['activity_type']=="TRANSFERS") | (group['activity_type']=="WIRE TRANSFER") | (group['activity_type']=="BILL PAYMENT")| (group['activity_type']=="CREDIT CARD PAYMENT")])
    df_dict['Total_Branch_Online_Activities'] = len(group[(group['channel']=="TRANSFERS") | (group['activity_type']=="TRANSFERS") | (group['activity_type']=="WIRE TRANSFER")| (group['activity_type']=="BILL PAYMENT")| (group['activity_type']=="CREDIT CARD PAYMENT")])                                   
    df_dict['Branch_Usage_Ratio_For_Online'] = df_dict['Total_Branch_Online_Activities']/group["Total_Online_Activities"] 

    df_dict['Total_IB_Transactions'] = len(group[(group['channel']=="INTERNET BANKING")])
    df_dict['Total_MB_Transactions'] = len(group[(group['channel']=="MOBILE BANKING")])
    df_dict['Total_PB_Transactions'] = len(group[(group['channel']=="PHONE BANKING")])
    df_dict['Total_ATM_Transactions'] = len(group[(group['channel']=="ATM")])
    df_dict['Total_ITM_Transactions'] = len(group[(group['channel']=="ITM")])

    df_dict['Total_Branch_Offline_Activities'] = len(group[(group['channel']=="BRANCH") | (group['activity_type']=="CASH DEPOSIT") | (group['activity_type']=="CHEQUE DEPOSIT")| (group['activity_type']=="CASH WITHDRAWAL") | (group['activity_type']=="CHEQUE WITHDRAWAL")])
    
    df_dict['Total_Branch_CHEQUES_Activities'] = len(group[(group['activity_type']=="CHEQUES")])

    df_dict['Total_ATM_Online_Activities'] = len(group[(group['channel']=="ATM") & (group['activity_type']=="TRANSFERS") | (group['activity_type']=="BILL PAYMENT") | (group['activity_type']=="CREDIT CARD PAYMENT")])

    df_dict['Total_ATM_Offline_Activities'] = len(group[(group['channel']=="ATM") & (group['activity_type']=="CASH DEPOSIT") | (group['activity_type']=="CASH WITHDRAWAL")])

    df_dict['Total_ITM_Online_Activities'] = len(group[(group['channel']=="ITM") & (group['activity_type']=="TRANSFERS") | (group['activity_type']=="BILL PAYMEN")| (group['activity_type']=="CREDIT CARD PAYMENT")])

    df_dict['Total_ITM_Offline_Activities'] = len(group[(group['channel']=="ITM") & (group['activity_type']=="CASH DEPOSIT") | (group['activity_type']=="CASH WITHDRAWAL")| (group['activity_type']=="CHEQUE DEPOSIT") | (group['activity_type']=="CHEQUE WITHDRAWAL")])

    df_dict['Total_Branch_Atm_Activities'] = len(group[(group['channel']=="BRANCH") & (group['activity_type']=="TRANSFERS") | (group['activity_type']=="BILL PAYMENT")| (group['activity_type']=="CREDIT CARD PAYMENT") | (group['activity_type']=="CASH DEPOSIT") | (group['activity_type']=="CASH WITHDRAWAL")])

    df_dict['Total_IB_Atm_Activities'] = len(group[(group['channel']=="INTERNET BANKING") & (group['activity_type']=="TRANSFERS") | (group['activity_type']=="BILL PAYMENT")| (group['activity_type']=="CREDIT CARD PAYMENT")])

    df_dict['Total_MB_Atm_Activities'] = len(group[(group['channel']=="MOBILE BANKING") & (group['activity_type']=="TRANSFERS") | (group['activity_type']=="BILL PAYMENT")| (group['activity_type']=="CREDIT CARD PAYMENT")])
    
    df_dict['Total_ITM_Atm_Activities'] = len(group[(group['channel']=="MOBILE BANKING") & (group['activity_type']=="TRANSFERS") | (group['activity_type']=="BILL PAYMENT")| (group['activity_type']=="CREDIT CARD PAYMENT")])

    df_dict['Total_PB_Atm_Activities'] = len(group[(group['channel']=="PHONE BANKING") & (group['activity_type']=="BILL PAYMENT")| (group['activity_type']=="CREDIT CARD PAYMENT")])

    df_dict['Total_Cash_Deposit_Withdrawals'] = len(group[(group['activity_type']=="CASH DEPOSIT")| (group['activity_type']=="CASH WITHDRAWAL")])

    df_dict['Total_Branch_Atm_Offline_Activities'] = len(group[(group['channel']=="BRANCH") & (group['activity_type']=="CASH DEPOSIT")| (group['activity_type']=="CASH WITHDRAWAL")])

    df_dict['Branch_Usage_Ratio_For_ATM'] = df_dict['Total_Branch_Atm_Offline_Activities'] / group['Total_Cash_Deposit_Withdrawals']

    df_dict['Total_ITM_Atm_Offline_Activities'] = len(group[(group['channel']=="ITM") & (group['activity_type']=="CASH DEPOSIT")| (group['activity_type']=="CASH WITHDRAWA")])

    date_df = pd.DataFrame([df_dict])
    last_date_df = last_date_df.append(date_df)


allData = pd.concat([last_date_df, customerDemographic], axis=1)

allData['Branch_Visit_Freq_Monthly'] = allData['Branch_Visit_Freq_Monthly']/allData['months_active']
